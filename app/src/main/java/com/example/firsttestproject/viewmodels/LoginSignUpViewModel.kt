package com.example.firsttestproject.viewmodels

import android.content.Context
import android.widget.Toast
import androidx.lifecycle.ViewModel
import com.example.firsttestproject.project.*
import com.google.gson.Gson
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response

class LoginSignUpViewModel : ViewModel() {
    private val myDataList = ArrayList<MyData>()
    private val gson = Gson()
    private val request = Request()
    private val loginSignupManager = LoginSignupManager()
    private val settingsManager = SettingsManager()

    suspend fun loadImg(context: Context?, callback: (SettingsModel?) -> Unit) {
        request.doRequestOkHttp(object : OnResponseInterface {
            override fun onResponse(response: Response<ResponseBody?>, body: ResponseBody) {
                val json = body.string()
                try {
                    val settings = Gson().fromJson(json, SettingsModel::class.java)
                    callback(settings)
                } catch (e: Exception) {
                    Toast.makeText(context, "Something went wrong", Toast.LENGTH_LONG).show()
                    callback(null)
                }
            }

            override fun onFailure(call: Call<ResponseBody?>) {
                callback(null)
            }
        }, settingsManager.getSettings(), context)
    }


    suspend fun login(
        email: String,
        password: String,
        fieldsMap: HashMap<String, String>,
        sharedPreferencesUtil: SharedPrefUtils,
        context: Context?,
        callback: (LoginModel?) -> Unit
    ) {

        if (email.isNotEmpty() && password.isNotEmpty()) {
            request.doRequestOkHttp(object : OnResponseInterface {
                override fun onResponse(
                    response: Response<ResponseBody?>,
                    body: ResponseBody
                ) {
                    val json = body.string()
                    try {
                        val login = Gson().fromJson(json, LoginModel::class.java)
                        callback(login)
                        myDataList.add(
                            MyData(
                                login.phone,
                                login.firstName,
                                login.lastName
                            )
                        )
                        val newJson = gson.toJson(myDataList)
                        sharedPreferencesUtil.saveString(MY_DATA_LIST, newJson)
                    } catch (e: java.lang.Exception) {
                        Toast.makeText(
                            context,
                            "Something With Wrong",
                            Toast.LENGTH_LONG
                        ).show()
                        callback(null)
                    }

                }

                override fun onFailure(call: Call<ResponseBody?>) {
                    callback(null)
                }
            }, loginSignupManager.loginCall(fieldsMap), context)
        }
    }


    suspend fun registerCall(
        fieldsMap: HashMap<String, String>,
        context: Context?,
        checkIsTrue: Boolean
    ) {
        if (checkIsTrue) {
            request.doRequestOkHttp(object : OnResponseInterface {
                override fun onResponse(response: Response<ResponseBody?>, body: ResponseBody) {
//                    TODO("when it is necessary to perform an action in the registration response")
//                    val json = body.string()
//                    try {
//                        val register = Gson().fromJson(json, RegisterModel::class.java)
//                    } catch (e: java.lang.Exception) {
//                        Toast.makeText(
//                            context,
//                            "Something With Wrong",
//                            Toast.LENGTH_LONG
//                        ).show()
//                    }
                }

                override fun onFailure(call: Call<ResponseBody?>) {}
            }, loginSignupManager.registerCall(fieldsMap), context)
        }
    }

}