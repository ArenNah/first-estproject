package com.example.firsttestproject.views

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.example.firsttestproject.databinding.LoginPageFragmentBinding
import com.example.firsttestproject.project.*
import com.example.firsttestproject.viewmodels.LoginSignUpViewModel
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch


class LoginPageFragment : Fragment() {
    private lateinit var binding: LoginPageFragmentBinding
    private lateinit var click: FragmentClickListener
    private lateinit var showToolBarListener: ShowToolBarListener
    private lateinit var sharedPreferencesUtil: SharedPrefUtils
    private val loginSignUpViewModel = LoginSignUpViewModel()

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is FragmentClickListener) {
            click = context
        }
        if (context is ShowToolBarListener) {
            showToolBarListener = context
        }
    }

    @OptIn(DelicateCoroutinesApi::class)
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        sharedPreferencesUtil = SharedPrefUtils(requireContext())

        binding = LoginPageFragmentBinding.inflate(layoutInflater)

        GlobalScope.launch {
            loginSignUpViewModel.loadImg(context) { settings ->
                if (settings != null) {
                    Glide.with(requireContext())
                        .load(settings.img_base_url.plus(settings.media?.verticalLogo))
                        .into(binding.iconView)
                }
            }
        }

        binding.registerButtonInPageLogin.setOnClickListener {
            click.openFragment(RegisterPageFragment())
        }

        binding.buttonLogin.setOnClickListener {
            val fieldsMap: HashMap<String, String> = HashMap()
            fieldsMap["username"] = binding.emailEditTxtInPageLogin.text.toString()
            fieldsMap["password"] = binding.passwordEditTxtInPageLogin.text.toString()

            GlobalScope.launch {
                loginSignUpViewModel.login(
                    email = binding.emailEditTxtInPageLogin.text.toString(),
                    password = binding.passwordEditTxtInPageLogin.text.toString(),
                    fieldsMap = fieldsMap,
                    sharedPreferencesUtil = sharedPreferencesUtil,
                    context = context,
                ) { login ->
                    if (login != null) {
                        if (login.token.isNotBlank()) {
                            sharedPreferencesUtil.saveString(TOKEN, login.token)
                            showToolBarListener.showToolBarListener(View.VISIBLE)
                            click.openFragment(AddContactFragment())
                        }
                    }
                }
            }
        }
        return binding.root

    }
}
