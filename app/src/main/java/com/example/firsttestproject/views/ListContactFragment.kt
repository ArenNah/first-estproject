package com.example.firsttestproject.views

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.firsttestproject.databinding.ListContactFragmentBinding
import com.example.firsttestproject.project.*
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class ListContactFragment : Fragment() {

    private lateinit var binding: ListContactFragmentBinding
    private lateinit var click: FragmentClickListener

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is FragmentClickListener) {
            click = context
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = ListContactFragmentBinding.inflate(layoutInflater)

        val sharedPrefUtils = SharedPrefUtils(requireContext())
        val json = sharedPrefUtils.getString(MY_DATA_LIST, EMPTY_VALUE)
        val gson = Gson()
        val type = object : TypeToken<ArrayList<MyData>>() {}.type
        val myDataList = gson.fromJson<ArrayList<MyData>>(json, type) ?: arrayListOf()

        binding.recyclerView.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = MyAdapter(myDataList)
        }

        return binding.root
    }
}