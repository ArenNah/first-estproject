package com.example.firsttestproject.views

import android.content.Context
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.firsttestproject.R
import com.example.firsttestproject.databinding.RegisterPageFragmentBinding
import com.example.firsttestproject.project.EMAIL_PATTERN
import com.example.firsttestproject.project.FragmentClickListener
import com.example.firsttestproject.project.PASSWORD_PATTERN
import com.example.firsttestproject.viewmodels.LoginSignUpViewModel
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class RegisterPageFragment : Fragment() {

    private lateinit var binding: RegisterPageFragmentBinding
    private lateinit var click: FragmentClickListener
    private var checkIsTrue = true
    private val loginSignUpViewModel = LoginSignUpViewModel()


    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is FragmentClickListener) {
            click = context
        }
    }

    @OptIn(DelicateCoroutinesApi::class)
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = RegisterPageFragmentBinding.inflate(layoutInflater)


        val fieldsMap: HashMap<String, String> = HashMap()
        fieldsMap["first_name"] = binding.firstNameEditTxtInPageReg.text.toString()
        fieldsMap["last_name"] = binding.lastNameEditTxtInPageReg.text.toString()
        fieldsMap["email"] = binding.emailEditTxtInPageReg.text.toString()
        fieldsMap["phone"] = binding.phoneEditTxtInPageReg.text.toString()
        fieldsMap["password"] = binding.passwordEditTxtInPageReg.text.toString()
        fieldsMap["digits"] = "6"

        binding.apply {
            iconBackInPageReg.setOnClickListener {
                click.openFragment(LoginPageFragment())
            }
            loginButtonInPageReg.setOnClickListener {
                click.openFragment(LoginPageFragment())
            }
            registerButtonInPageReg.setOnClickListener {
                checkEmptyAndPattern()
            }
            GlobalScope.launch {
                loginSignUpViewModel.registerCall(
                    fieldsMap = fieldsMap,
                    context = context,
                    checkIsTrue
                )
            }

        }
        return binding.root
    }

    private fun checkEmptyAndPattern() {
        if (TextUtils.isEmpty(binding.firstNameEditTxtInPageReg.text.toString())) {
            binding.firstNameTxtInputInPageReg
                .error =
                resources.getString(R.string.emptyError)
            checkIsTrue = false
        } else {
            binding.firstNameTxtInputInPageReg
                .isErrorEnabled = false
        }

        if (TextUtils.isEmpty(binding.lastNameEditTxtInPageReg.text.toString())) {
            binding.lastNameTxtInputInPageReg.error =
                resources.getString(R.string.emptyError)
            checkIsTrue = false
        } else {
            binding.lastNameTxtInputInPageReg.isErrorEnabled = false
        }

        if (TextUtils.isEmpty(binding.emailEditTxtInPageReg.text.toString())) {
            binding.emailTxtInputInPageReg.error =
                resources.getString(R.string.emptyError)
            checkIsTrue = false
        } else if (!binding.emailEditTxtInPageReg.text.toString().trim()
                .matches(EMAIL_PATTERN.toRegex())
        ) {
            binding.emailTxtInputInPageReg.error =
                resources.getString(R.string.inCorrectEmail)
            checkIsTrue = false
        } else {
            binding.emailTxtInputInPageReg.isErrorEnabled = false
        }

        if (TextUtils.isEmpty(binding.passwordEditTxtInPageReg.text.toString())) {
            binding.passwordTxtInputInPageReg.error =
                resources.getString(R.string.emptyError)
            checkIsTrue = false
        } else {
            binding.passwordTxtInputInPageReg.isErrorEnabled = false

        }
        if (binding.passwordEditTxtInPageReg.text.toString().trim()
                .matches(PASSWORD_PATTERN.toRegex())
        ) {
            binding.passwordTxtInputInPageReg.error =
                resources.getString(R.string.incorrectPassword)
            checkIsTrue = false
        } else {
            binding.passwordTxtInputInPageReg.isErrorEnabled = false
        }

        if (TextUtils.isEmpty(binding.phoneEditTxtInPageReg.text.toString())) {
            binding.phoneTxtInputInPageReg.error =
                resources.getString(R.string.emptyError)
            checkIsTrue = false
        } else {
            binding.phoneTxtInputInPageReg.isErrorEnabled = false
        }

        if (TextUtils.isEmpty(binding.confirmEditTxtInPageReg.text.toString())) {
            binding.confirmTxtInputInPageReg.error =
                resources.getString(R.string.emptyError)
            checkIsTrue = false
        } else if (binding.confirmEditTxtInPageReg.text.toString() != binding.passwordEditTxtInPageReg.text.toString()) {
            binding.confirmTxtInputInPageReg.error =
                resources.getString(R.string.passwordDoesNotMatch)
            checkIsTrue = false
        } else {
            binding.confirmTxtInputInPageReg.isErrorEnabled = false
        }
    }


}