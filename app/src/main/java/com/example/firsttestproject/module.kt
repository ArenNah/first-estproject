package com.example.firsttestproject

import com.example.firsttestproject.viewmodels.LoginSignUpViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel {
        LoginSignUpViewModel()
    }
}