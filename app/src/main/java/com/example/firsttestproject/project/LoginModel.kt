package com.example.firsttestproject.project

import com.google.gson.annotations.SerializedName

data class LoginModel(
    @SerializedName("id")
    val id: String?,
    @SerializedName("account_id")
    val accountId: String?,
    @SerializedName("associated_id")
    val associatedId: String,
    @SerializedName("group_id")
    val groupId: String,
    @SerializedName("website_id")
    val websiteId: String,
    @SerializedName("parent_id")
    val parentId: String,
    @SerializedName("commission_id")
    val commissionId: String,
    @SerializedName("tier_id")
    val tierId: String,
    @SerializedName("pipeline_id")
    val pipelineId: String,
    @SerializedName("pipeline_status")
    val pipelineStatus: String,
    @SerializedName("partner")
    val partner: String,
    @SerializedName("paid")
    val paid: String,
    @SerializedName("points")
    val points: String,
    @SerializedName("badges")
    val badges: String,
    @SerializedName("shareable")
    val shareable: String,
    @SerializedName("admin")
    val admin: String,
    @SerializedName("profile_picture")
    val profilePicture: String,
    @SerializedName("profile_source")
    val profileSource: String,
    @SerializedName("type")
    val type: String,
    @SerializedName("type_sub")
    val typeSub: String,
    @SerializedName("data")
    val data: Any,
    @SerializedName("category")
    val category: String,
    @SerializedName("employee")
    val employee: String,
    @SerializedName("company_name")
    val companyName: String,
    @SerializedName("first_name")
    val firstName: String,
    @SerializedName("middle_name")
    val middleName: String,
    @SerializedName("last_name")
    val lastName: String,
    @SerializedName("title")
    val title: String,
    @SerializedName("email")
    val email: String,
    @SerializedName("username")
    val username: String,
    @SerializedName("phone")
    val phone: String,
    @SerializedName("phone2_name")
    val phone2Name: String,
    @SerializedName("phone2")
    val phone2: String,
    @SerializedName("phone3_name")
    val phone3Name: String,
    @SerializedName("phone3")
    val phone3: String,
    @SerializedName("address")
    val address: String,
    @SerializedName("address2")
    val address2: String,
    @SerializedName("city")
    val city: String,
    @SerializedName("state")
    val state: String,
    @SerializedName("zip_code")
    val zipCode: String,
    @SerializedName("store_zip")
    val storeZip: String,
    @SerializedName("country")
    val country: String,
    @SerializedName("birthdate")
    val birthdate: String,
    @SerializedName("gender")
    val gender: String,
    @SerializedName("height")
    val height: String,
    @SerializedName("weight")
    val weight: String,
    @SerializedName("dob_m")
    val dobM: String,
    @SerializedName("dob_d")
    val dobD: String,
    @SerializedName("dob_y")
    val dobY: String,
    @SerializedName("bio")
    val bio: String,
    @SerializedName("newsletter")
    val newsletter: String,
    @SerializedName("license")
    val license: String,
    @SerializedName("facebook")
    val facebook: String,
    @SerializedName("twitter")
    val twitter: String,
    @SerializedName("linkedin")
    val linkedin: String,
    @SerializedName("instagram")
    val instagram: String,
    @SerializedName("google_plus")
    val googlePlus: String,
    @SerializedName("pinterest")
    val pinterest: String,
    @SerializedName("snapchat")
    val snapchat: String,
    @SerializedName("yelp")
    val yelp: String,
    @SerializedName("last_active")
    val lastActive: String,
    @SerializedName("last_login")
    val lastLogin: String,
    @SerializedName("onboarded")
    val onboarded: String,
    @SerializedName("created")
    val created: String,
    @SerializedName("referred_by")
    val referredBy: String,
    @SerializedName("ssn")
    val ssn: String,
    @SerializedName("dln")
    val dln: String,
    @SerializedName("dln_exp")
    val dlnExp: String,
    @SerializedName("website")
    val website: String,
    @SerializedName("wage")
    val wage: String,
    @SerializedName("notes")
    val notes: String,
    @SerializedName("current_location")
    val currentLocation: String,
    @SerializedName("lat")
    val lat: String,
    @SerializedName("lng")
    val lng: String,
    @SerializedName("rotation")
    val rotation: String,
    @SerializedName("online")
    val online: String,
    @SerializedName("rating")
    val rating: String,
    @SerializedName("invitation")
    val invitation: String,
    @SerializedName("updated")
    val updated: String,
    @SerializedName("datetime")
    val datetime: String,
    @SerializedName("status")
    val status: String,
    @SerializedName("token")
    val token: String,
    @SerializedName("registered")
    val registered: String,
    @SerializedName("suspended")
    val suspended: String,
    @SerializedName("hold")
    val hold: String,
    @SerializedName("waiting_list")
    val waitingList: String,
    @SerializedName("sms_verified")
    val smsVerified: String,
    @SerializedName("country_code")
    val countryCode: String,
    @SerializedName("amlkyc")
    val amlkyc: String,
    @SerializedName("id_verified")
    val idVerified: String,
    @SerializedName("insurance_verified")
    val insuranceVerified: String,
)
