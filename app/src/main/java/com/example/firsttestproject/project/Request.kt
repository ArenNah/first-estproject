package com.example.firsttestproject.project

import android.content.Context
import android.widget.Toast
import com.google.gson.Gson
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

class Request {
    suspend fun doRequestOkHttp(
        listener: OnResponseInterface,
        call: Call<ResponseBody?>?,
        context: Context?
    ): Response<ResponseBody?> = suspendCoroutine { continuation ->
        call?.enqueue(object : retrofit2.Callback<ResponseBody?> {

            override fun onResponse(call: Call<ResponseBody?>, response: Response<ResponseBody?>) {
                if (response.isSuccessful) {
                    val body = response.body()
                    if (body != null) {
                        listener.onResponse(response, body)
                    } else {
                        try {
                            val json = response.errorBody()?.string()
                            val errorResponse = Gson().fromJson(json, ErrorModel::class.java)
                            if (errorResponse != null) {
                                Toast.makeText(
                                    context,
                                    "Error:  ${errorResponse.error?.message}",
                                    Toast.LENGTH_LONG
                                ).show()
                            }
                        } catch (e: java.lang.Exception) {
                            Toast.makeText(
                                context,
                                "Something With Wrong",
                                Toast.LENGTH_LONG
                            ).show()
                        }
                    }
                } else {
                    try {
                        val json = response.errorBody()?.string()
                        val errorResponse = Gson().fromJson(json, ErrorModel::class.java)
                        if (errorResponse != null) {
                            Toast.makeText(
                                context,
                                "Error: ${errorResponse.error?.message}",
                                Toast.LENGTH_LONG
                            ).show()
                        }
                    } catch (e: java.lang.Exception) {
                        Toast.makeText(
                            context,
                            "Something With Wrong",
                            Toast.LENGTH_LONG
                        ).show()
                    }

                }
                continuation.resume(response)
            }

            override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {
                call.cancel()
            }
        })
    }
}

interface OnResponseInterface {
    fun onResponse(response: Response<ResponseBody?>, body: ResponseBody)
    fun onFailure(call: Call<ResponseBody?>)
}

