package com.example.firsttestproject.project

import okhttp3.ResponseBody
import retrofit2.Call

class SettingsManager {
    fun getSettings(): Call<ResponseBody?>? {
        return apiInterface?.getSettings(URL_KEY)
    }
}