package com.example.firsttestproject.project

import com.google.gson.annotations.SerializedName

data class Media(
    @SerializedName("verticalLogo")
    val verticalLogo: String?,

)
