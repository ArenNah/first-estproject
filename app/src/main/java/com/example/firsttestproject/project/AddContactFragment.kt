package com.example.firsttestproject.project


import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.provider.ContactsContract
import android.provider.Settings
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.example.firsttestproject.databinding.AddContactFragmentBinding
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken


class AddContactFragment : Fragment() {

    private lateinit var binding: AddContactFragmentBinding
    private lateinit var click: FragmentClickListener

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is FragmentClickListener) {
            click = context
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = AddContactFragmentBinding.inflate(layoutInflater)

        val sharedPreferencesUtil = SharedPrefUtils(requireContext())
        val json = sharedPreferencesUtil.getString(MY_DATA_LIST, EMPTY_VALUE)
        val gson = Gson()
        val type = object : TypeToken<ArrayList<MyData>>() {}.type
        val myDataList = gson.fromJson<ArrayList<MyData>>(json, type) ?: arrayListOf()

        binding.saveButtonInPageMain.setOnClickListener {
            val myData = MyData(
                binding.phoneNumberInPageMain.text.toString(),
                binding.firstNameInPageMain.text.toString(),
                binding.lastNameInPageMain.text.toString()
            )
            myDataList.add(myData)
            val newJson = gson.toJson(myDataList)
            sharedPreferencesUtil.saveString(MY_DATA_LIST, newJson)

            Toast.makeText(
                requireContext(),
                "User Saved in the contact ${myDataList.size}",
                Toast.LENGTH_SHORT
            ).show()
        }

        binding.iconPhoneBookInPageMain.setOnClickListener {
            if (!this.shouldShowRequestPermissionRationale(Manifest.permission.READ_CONTACTS)
            ) {
                requestPermissionInReadContacts()
            } else {
                resultInReadContacts.launch(Manifest.permission.READ_CONTACTS)
            }
        }
        return binding.root
    }

    private var resultLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                val data: Intent? = result.data
                takePhoneNumberInPhoneBook(data)
            }
        }

    private val resultInReadContacts =
        registerForActivityResult(
            ActivityResultContracts.RequestPermission()
        ) { isGranted: Boolean ->
            if (isGranted) {
                requestPermissionInReadContacts()
            } else {
                settingsIntent()
            }
        }

    private fun requestPermissionInReadContacts() {
        if (ContextCompat.checkSelfPermission(
                requireContext(), Manifest.permission.READ_CONTACTS
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            getPhoneNumberFromPhonebook()
        } else if (ContextCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.READ_CONTACTS
            )
            == PackageManager.PERMISSION_DENIED
        ) {
            ActivityCompat.requestPermissions(
                requireActivity(),
                arrayOf(Manifest.permission.READ_CONTACTS),
                OKHTTP
            )
        }
    }

    private fun takePhoneNumberInPhoneBook(data: Intent?) {

        val contactUri = data?.data
        val projection = arrayOf(ContactsContract.CommonDataKinds.Phone.NUMBER)
        val cursor =
            contactUri?.let {
                requireActivity().contentResolver.query(
                    it,
                    projection,
                    null,
                    null,
                    null
                )
            }
        if (cursor != null && cursor.moveToFirst()) {
            val columnIndex =
                cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)
            val getPhoneNumber = cursor.getString(columnIndex)
            binding.phoneNumberInPageMain.setText(getPhoneNumber)
        }
        cursor?.close()
    }

    private val startResultActivity = registerForActivityResult(
        ActivityResultContracts.StartActivityForResult()
    ) {}

    @SuppressLint("IntentReset")
    private fun getPhoneNumberFromPhonebook() {
        val intent = Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI)
        intent.type = ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE
        resultLauncher.launch(intent)
    }

    private fun settingsIntent() {
        val openSettingsIntent = Intent(
            Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
            Uri.parse("package:" + requireContext().packageName)
        )
        startResultActivity.launch(openSettingsIntent)
    }
}

