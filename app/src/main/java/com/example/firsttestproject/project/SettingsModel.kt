package com.example.firsttestproject.project

import com.google.gson.annotations.SerializedName

data class SettingsModel(
    @SerializedName("media")
    val media: Media?,
    @SerializedName("img_base_url")
    val img_base_url: String?,
)
