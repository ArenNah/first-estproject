package com.example.firsttestproject.project

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.firsttestproject.R

class MyAdapter(private val mData: ArrayList<MyData>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == HEADER_TYPE) {
            val headerView =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.header_list_contact, parent, false)
            HeaderViewHolder(headerView)
        } else {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_view_list_contact, parent, false)
            ViewHolder(view)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (getItemViewType(position) == ITEM_TYPE) {
            val item = mData[position - 1]
            (holder as ViewHolder).phoneNumber.text = item.phoneNumber
            holder.firstName.text = item.firstName
            holder.lastName.text = item.lastName
        }
    }

    override fun getItemCount(): Int {
        return mData.size + 1
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == 0) {
            HEADER_TYPE
        } else {
            ITEM_TYPE
        }
    }

    inner class HeaderViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val phoneNumberHeader: TextView
        private val headerFirstName: TextView
        private val headerLastName: TextView

        init {
            phoneNumberHeader = itemView.findViewById(R.id.header_phone_number)
            headerFirstName = itemView.findViewById(R.id.header_first_name)
            headerLastName = itemView.findViewById(R.id.header_last_name)
        }
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val phoneNumber: TextView
        val firstName: TextView
        val lastName: TextView

        init {
            phoneNumber = itemView.findViewById(R.id.phone_number_in_item_view)
            firstName = itemView.findViewById(R.id.first_name_in_item_view)
            lastName = itemView.findViewById(R.id.last_name_in_item_view)
        }
    }
}