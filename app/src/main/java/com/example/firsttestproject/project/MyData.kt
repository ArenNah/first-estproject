package com.example.firsttestproject.project

data class MyData(
    val phoneNumber: String,
    val firstName: String,
    val lastName: String
)