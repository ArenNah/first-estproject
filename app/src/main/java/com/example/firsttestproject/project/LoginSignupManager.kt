package com.example.firsttestproject.project

import okhttp3.ResponseBody
import retrofit2.Call

val apiInterface = APIClient().getClient()?.create(APIInterface::class.java)


class LoginSignupManager {
    fun loginCall(fieldsMap: HashMap<String, String>): Call<ResponseBody?>? {
        return apiInterface?.login(
            URL_KEY, fieldsMap
        )
    }

    fun registerCall(fieldsMap: HashMap<String, String>): Call<ResponseBody?>? {
        return apiInterface?.register(
            URL_KEY, fieldsMap
        )
    }


}