package com.example.firsttestproject.project

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

interface APIInterface {
    @GET("website/settings")
    fun getSettings(@Query("key") key: String?): Call<ResponseBody?>?

    @FormUrlEncoded
    @POST("users/login")
    fun login(
        @Query("key") key: String?,
        @FieldMap fieldsMap: HashMap<String, String>
    ): Call<ResponseBody?>?

    @FormUrlEncoded
    @POST("users/register")
    fun register(
        @Query("key") key: String?,
        @FieldMap fieldsMap: HashMap<String, String>
    ): Call<ResponseBody?>?
}