package com.example.firsttestproject.project

import com.google.gson.annotations.SerializedName

data class ErrorModel(
    @SerializedName("error")
    val error: Error?,
)

