package com.example.firsttestproject.project


const val URL_KEY = "098WXjnytf!rFN0lX7RinOA2hz@KkeDloMei2CRwRPXtzXPu1DMppkrGTqobF@w0"
const val BASE_URL = "https://api.logistically.io/api/v2/"
const val EMAIL_PATTERN = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+"
const val PASSWORD_PATTERN = "^" + "(?=.*[0" + "--9])" + "(?=.*[A-Z])" + "(?=.*[a-zA-Z])" + "(?=\\S+$)" + ".{6,}" + "$"
const val MY_DATA_LIST = "my_data_list"
const val MY_USER_PREF = "MyUserPref"
const val OKHTTP = 200
const val HEADER_TYPE = 0
const val ITEM_TYPE = 1
const val TOKEN = "token"
const val EMPTY_VALUE = ""



