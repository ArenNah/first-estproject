package com.example.firsttestproject.project

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.example.firsttestproject.R
import com.example.firsttestproject.databinding.MainActivityBinding
import com.example.firsttestproject.views.ListContactFragment
import com.example.firsttestproject.views.LoginPageFragment


class MainActivity : AppCompatActivity(), FragmentClickListener, ShowToolBarListener {
    private lateinit var binding: MainActivityBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = MainActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val sharedPreferencesUtil = SharedPrefUtils(this)
        val token = sharedPreferencesUtil.getString(TOKEN, EMPTY_VALUE)
        var isState = true

        if (token != null) {
            openFragment(AddContactFragment())
            binding.toolbar.visibility = View.VISIBLE
        } else {
            binding.toolbar.visibility = View.GONE
            openFragment(LoginPageFragment())
        }

        binding.listContactIconInToolbar.setOnClickListener {
            if (isState) {
                openFragment(ListContactFragment())
                isState = false
                binding.toolbar.visibility = View.VISIBLE
            } else {
                openFragment(AddContactFragment())
                binding.toolbar.visibility = View.VISIBLE
                isState = true
            }
        }

        binding.logOutIconInToolbar.setOnClickListener {
            sharedPreferencesUtil.clearSharedPref()
            binding.toolbar.visibility = View.GONE
            openFragment(LoginPageFragment())
        }
    }


    override fun showToolBarListener(value: Int) {
        binding.toolbar.visibility = value
    }

    override fun openFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
            .replace(R.id.open_fragment_container, fragment)
            .commit()
    }

}

interface FragmentClickListener {
    fun openFragment(fragment: Fragment)

}

interface ShowToolBarListener {
    fun showToolBarListener(value: Int)

}
